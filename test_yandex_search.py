from allure_commons.types import Severity
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
import allure

@allure.title('Yandex')
@allure.severity(Severity.BLOCKER)
def test_yandex_search():
    driver = webdriver.Chrome(executable_path=r'C:\selenium\chromedriver.exe')
    with allure.step('0'):
        driver.get('https://ya.ru')

    with allure.step('3'):
        search_input = driver.find_element_by_xpath('//*[@id="text"]')
        search_button = driver.find_element_by_xpath('//div[@class="search2__button"]//button[@type="submit"]')
        search_input.send_keys('market.yandex.ru')
        search_button.click()

    def check_results_count(driver):
        inner_search_results = driver.find_element_by_xpath('//li[@class="serp-item"]')
        return len(inner_search_results) == 12
    with allure.step('1'):
         WebDriverWait(driver, 5, 0.5).until(check_results_count)
    with allure.step('4'):
        search_results = driver.find_element_by_xpath('//li[@class="serp-item"]')
        link = search_results[1].find_element_by_xpath('.//h2/a')
        link.click()
    driver.switch_to.window(driver.window_handles[1])
    with allure.step():
        assert driver.title == "2"

